﻿using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Otus.Teaching.PromoCodeFactory.DataAccess.Repositories
{
	public class InMemoryRepository<T>
		: IRepository<T>
		where T : BaseEntity
	{
		protected IList<T> Data { get; set; }

		public InMemoryRepository(IEnumerable<T> data)
		{
			Data = data.ToList();
		}

		public Task<IEnumerable<T>> GetAllAsync()
		{
			return Task.FromResult((IEnumerable<T>)Data);
		}

		public Task<T> GetByIdAsync(Guid id)
		{
			return Task.FromResult(Data.FirstOrDefault(x => x.Id == id));
		}

		public Task Create(T entity)
		{
			Data.Add(entity);

			return Task.CompletedTask;
		}

		public Task Delete(T entity)
		{
			if (entity == null)
				throw new ArgumentException("There is no such element in the list");

			Data.Remove(entity);

			return Task.CompletedTask;
		}

		public Task Update(T entity)
		{
			if (entity == null)
				throw new NullReferenceException(nameof(entity));

			var index = Data.Select((v, i) => new { ent = v, index = i })
				.First( v => v.ent.Id == entity.Id).index;

			if (index == -1)
				throw new ArgumentException("There is no such element in the list");

			Data[index] = entity;

			return Task.CompletedTask;
		}
	}
}
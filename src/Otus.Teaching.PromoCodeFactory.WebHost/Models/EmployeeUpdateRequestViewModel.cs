﻿using Otus.Teaching.PromoCodeFactory.Core.Domain;
using System.ComponentModel.DataAnnotations;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Models
{
	public class EmployeeUpdateRequestViewModel
		: BaseEntity
	{
		[Required]
		public string FirstName { get; set; }

		public string LastName { get; set; }
		public string Email { get; set; }
	}
}
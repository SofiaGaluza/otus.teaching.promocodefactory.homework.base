﻿using System.ComponentModel.DataAnnotations;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Models
{
	public class EmployeeCreateRequestViewModel
	{
		[Required]
		public string FirstName { get; set; }

		public string LastName { get; set; }
		public string Email { get; set; }
	}
}
﻿using Otus.Teaching.PromoCodeFactory.Core.Domain;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Models
{
	public class EmployeeShortViewModel
		: BaseEntity
	{
		public string FullName { get; set; }
		public string Email { get; set; }
	}
}
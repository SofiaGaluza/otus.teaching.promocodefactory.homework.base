﻿using Otus.Teaching.PromoCodeFactory.Core.Domain;
using System.Collections.Generic;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Models
{
	public class EmployeeViewModel
		: BaseEntity
	{
		public string FullName { get; set; }
		public string Email { get; set; }
		public List<RoleViewModel> Roles { get; set; }
		public int AppliedPromocodesCount { get; set; }
	}
}
﻿using Microsoft.AspNetCore.Mvc;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain.Administration;
using Otus.Teaching.PromoCodeFactory.WebHost.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Controllers
{
	/// <summary>
	/// Сотрудники
	/// </summary>
	[ApiController]
	[Route("api/v1/[controller]")]
	public class EmployeesController
		: ControllerBase
	{
		private readonly IRepository<Employee> _employeeRepository;

		public EmployeesController(IRepository<Employee> employeeRepository)
		{
			_employeeRepository = employeeRepository;
		}

		/// <summary>
		/// Получить данные всех сотрудников
		/// </summary>
		/// <returns></returns>
		[HttpGet]
		public async Task<List<EmployeeShortViewModel>> GetEmployeesAsync()
		{
			var employees = await _employeeRepository.GetAllAsync();

			var employeesModelList = employees.Select(x =>
				new EmployeeShortViewModel()
				{
					Id = x.Id,
					Email = x.Email,
					FullName = x.FullName,
				}).ToList();

			return employeesModelList;
		}

		/// <summary>
		/// Получить данные сотрудника по Id
		/// </summary>
		/// <returns></returns>
		[HttpGet("{id:guid}")]
		public async Task<ActionResult<EmployeeViewModel>> GetEmployeeByIdAsync(Guid id)
		{
			var employee = await _employeeRepository.GetByIdAsync(id);

			if (employee == null)
				return NotFound();

			var employeeModel = new EmployeeViewModel()
			{
				Id = employee.Id,
				Email = employee.Email,
				Roles = employee.Roles.Select(x => new RoleViewModel()
				{
					Name = x.Name,
					Description = x.Description
				}).ToList(),
				FullName = employee.FullName,
				AppliedPromocodesCount = employee.AppliedPromocodesCount
			};

			return employeeModel;
		}

		[HttpPost]
		public async Task<ActionResult> CreateEmployeeAsync(EmployeeCreateRequestViewModel model)
		{
			await _employeeRepository.Create(new Employee()
			{
				FirstName = model.FirstName,
				LastName = model.LastName,
				Email = model.Email
			});

			return NoContent();
		}

		[HttpDelete]
		public async Task<ActionResult> DeleteEmployeeAsync(Guid id)
		{
			var employee = await _employeeRepository.GetByIdAsync(id);

			if (employee == null)
				return NotFound();

			await _employeeRepository.Delete(employee);

			return NoContent();
		}

		[HttpPut]
		public async Task<ActionResult> UpdateEmployeeAsync(EmployeeUpdateRequestViewModel model)
		{
			var employee = await _employeeRepository.GetByIdAsync(model.Id);

			if (employee == null)
				return NotFound();

			employee.FirstName = model.FirstName;
			employee.LastName = model.LastName;
			employee.Email = model.Email;

			await _employeeRepository.Update(employee);

			return NoContent();
		}
	}
}